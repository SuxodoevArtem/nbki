const express = require('express');
const cors = require('cors');
const http = require('http');

const scoring = require('./routers/scoring.router');

const app = express();
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true}));

app.use(scoring);

http.createServer({},app).listen(3000, () => {
    console.log(`Server running at 3000`);
})