const router = require('express-promise-router')();

const { scoring } = require('../controllers/index');

router.route('/scoring/ReqScoring').put(scoring.ReqScoring);
router.route('/scoring/GetReports').get(scoring.GetReports);

module.exports = router;