import Vue from 'vue'
import Vuex from 'vuex'
import auth from "./auth"
import scoring from "./scoring"


Vue.use(Vuex);

export default new Vuex.Store({
   modules: {
       auth,
       scoring,
   }
})