import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter);

const routes = [    
    {
        path: '/Login',
        name: 'Login',
        component: () => import('@/views/Login.vue'),
        meta: { requiresAuth: false } 
    },  
    {
        path: '/',
        name: '/Main',
        component: () => import('@/views/Main.vue'),
        //meta: { requiresAuth: false },
        children: [
            {
                path: '/Scoring',
                name: 'Scoring',
                component: () => import('@/views/views_scoring/Scoring.vue'),
                children: [
                    {
                        path: '/Scoring/MFO_V2',
                        name: 'MFO_V2',
                        component: () => import('@/views/views_scoring/MFO_V2.vue'),
                    },
                    {
                        path: '/Scoring/ScoringHistory',
                        name: 'ScoringHistory',
                        component: () => import('@/views/views_scoring/ScoringHistory.vue'),
                    },
                ]
            },
        ]
    },
]



const router = new VueRouter({
    mode: 'history',
    routes,
})

export default router;