import axios from 'axios';

const instance = axios.create({
    baseURL: 'http://localhost:3000'
});

export const ReqScoring = async (data) => {  
    
    const ScoringMFO_V2 = data;

    const response = await instance.put('/scoring/ReqScoring',ScoringMFO_V2);

    console.log(response.message);
    return response.message;

}

export const GetReports = async () => {
    const response = await instance.get('/scoring/GetReports');
    
    console.log(response.data);
    console.log(response.message);
    
    return response;
}